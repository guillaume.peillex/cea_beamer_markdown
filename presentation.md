---
title: Ma présentation
subtitle: Un sous-titre explicatif
author:
- Prénom NOM
date: 13 Juin 2023
theme: CEA2023
template: templateCEA2023
aspectratio: 169
---

# Section 1

## Point A

- Les puces
  - La première
  - La seconde
  - La dernières
- Un autre puce
  - La quatrième
  - La cinquième

## Point B

Voici un extrait de code:

    for (const auto& val : container)
    {
      std::cout << val << \n;
    }

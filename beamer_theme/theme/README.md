Mettre ici les fichiers LOGO_CEA_ORIGINAL.svg et LOGO_CEA_ROUGE_SUR_BLANC.svg.
Compatible avec les logos aux formats .svg, .png et .jpeg. Si un autre format
que le .svg est utilisé, il faut modifier les chemins dans le script Python.

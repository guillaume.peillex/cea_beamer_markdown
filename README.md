# CEA_BEAMER_MARKDOWN

## What is CEA_BEAMER_MARKDOWN

**CEA_BEAMER_MARKDOWN** allows to make beamer slides with CEA style guide using Markdown langage.

It uses the beamer theme created by Alexandre l'Heritier <alexandre.lheritier@cea.fr>.

## How to use it

First clone this repository:

    git clone git@gitlab.com:guillaume.peillex/cea_beamer_markdown.git

Descend into the new directory:

    cd cea_beamer_markdown

Create a build directory and descend into it:

    mkdir build
    cd build

Call CMake to configure the project:

    cmake -Dinput_filename=presentation.md ..

It is of course possible to change the name of the source file but the `input_filename` should
be modified in accordance.

Then build the presentation

    cmake --build .

## Requirements

**CEA_BEAMER_MARKDOWN** requires **pandoc** and **texlive**.
A pdf viewer is not necessary but may be usefull to view the result.

If **CEA_BEAMER_MARKDOWN** find a viewer (among **evince** or **okular**), then 
a target named `View` is created and it is possible to build and view the result by simply
typing:

    cmake --build . --target View
